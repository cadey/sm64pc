{ pkgs ? import <nixpkgs> { } }:

with pkgs;

stdenv.mkDerivation rec {
  pname = "sm64pc";
  version = "latest";

  buildInputs = [
    gnumake
    python3
    audiofile
    pkg-config
    SDL2
    libusb1
    glfw3
    libgcc
    xorg.libX11
    xorg.libXrandr
    libpulseaudio
    alsaLib
    glfw
    libGL
    unixtools.hexdump
    clang_10
  ];

  src = ./.;

  buildPhase = ''
    chmod +x ./extract_assets.py
    make -j
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp ./build/us_pc/sm64.us.f3dex2e $out/bin/sm64pc
  '';

  meta = with stdenv.lib; {
    description = "Super Mario 64 PC port, requires rom :)";
    license = licenses.unfree;
  };
}
