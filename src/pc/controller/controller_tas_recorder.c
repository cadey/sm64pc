#include <stdio.h>
#include <ultra64.h>
#include <assert.h>
#include <fcntl.h>

#include "controller_api.h"

static FILE *fout;
static int counter;

#define OFFSET 0x400
#define fname "rec.m64"

static void tas_recorder_close(void) {
    fclose(fout);
    printf("[tas_recorder] saving tas data to %s\n", fname);
}

static void tas_recorder_init(void) {
    if (fname == NULL) {
        fout = NULL;
        return;
    }

    unlink(fname);
    printf("[tas_recorder] writing output to %s\n", fname);
    fout = fopen(fname, "wb");
    assert(fout != NULL);
    uint8_t buf[OFFSET];
    memset(buf, 0, sizeof(buf));
    fwrite(buf, 1, sizeof(buf), fout);
    atexit(tas_recorder_close);
    counter = 0;
}

static void tas_recorder_read(OSContPad *pad) {
    if (fout == NULL) {
        return;
    }
    counter += 4;

    uint8_t bytes[4] = {0};
    int button1 = pad->button;
    int button2 = pad->button;
    bytes[0] = button1 >> 8;
    bytes[1] = button2 & 0x00FF;
    bytes[2] = pad->stick_x;
    bytes[3] = pad->stick_y;
    fwrite(bytes, 1, 4, fout);

    printf("[tas_recorder] %08x: %04x %02x%02x\n", (counter + OFFSET), pad->button, bytes[2], bytes[3]);
}

struct ControllerAPI controller_tas_recorder = {
    tas_recorder_init,
    tas_recorder_read
};
