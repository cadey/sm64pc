#include <stdio.h>
#include <ultra64.h>
#include <assert.h>
#include <fcntl.h>

#include "controller_api.h"

static FILE *fin;
static int counter;

#define OFFSET 0x400
#define fname "cont.m64"

static void tas_init(void) {
    fin = fopen(fname, "rb");
    if (fin == NULL) {
        return;
    }

    printf("[tas_playback] loading %s\n", fname);
    uint8_t buf[OFFSET];
    fread(buf, 1, sizeof(buf), fin);
    counter = 0;
}

static void tas_read(OSContPad *pad) {
    if (fin == NULL) {
        return;
    }

    uint8_t bytes[4] = {0};
    int result = fread(bytes, 1, 4, fin);
    if (feof(fin)) {
        printf("[tas_playback] end of tas input\n");
        fclose(fin);
        fin = NULL;
        return;
    }

    pad->button = (bytes[0] << 8) | bytes[1];
    pad->stick_x = bytes[2];
    pad->stick_y = bytes[3];
    counter+=4;
    printf("[tas_playback] %08x called %04x %02x%02x\n", (counter + OFFSET), pad->button, bytes[2], bytes[3]);
    fflush(stdout);
}

struct ControllerAPI controller_recorded_tas = {
    tas_init,
    tas_read
};
