{ pkgs ? import <nixpkgs> { } }:

with pkgs;

let
  site = callPackage ./default.nix { };

  dockerImage = pkg:
    pkgs.dockerTools.buildLayeredImage {
      name = "ghcr.io/xe/sm64pc";
      tag = "latest";

      contents = [ pkg mesa mesa.drivers mesa_glu mesa_noglu libGL_driver ];

      config = {
        Cmd = [ "${strace}/bin/strace" "${pkg}/bin/sm64pc" ];
        Env = [ "LD_LIBRARY_PATH=/lib" "LIBGL_DRIVERS_PATH=/lib/dri" ];
        WorkingDir = "/";
      };
    };

in dockerImage site
