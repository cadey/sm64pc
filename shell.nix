let
  sources = import ./nix/sources.nix;
  pkgs = import <nixpkgs> { };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    # Mario 64
    python3
    audiofile
    pkg-config
    SDL2
    libusb1
    glfw3
    libgcc
    xorg.libX11
    xorg.libXrandr
    libpulseaudio
    alsaLib
    glfw
    libGL
    openssl

    # build tools
    gnumake
    clang_10
    gdb

    # gamebridge
    rustc
    cargo
    cargo-watch
    rls
    rustfmt
  ];
}
